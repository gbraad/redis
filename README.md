Redis Dockerfiles build wrapper
===============================

!["Prompt"](https://raw.githubusercontent.com/gbraad/assets/gh-pages/icons/prompt-icon-64.png)


Build wrapper for Redis Docker images.


Usage
-----

  * Redis 3.2 (Debian based)  
    `docker pull registry.gitlab.com/gbraad/redis:3.2`
  * ...


Authors
-------

| [!["Gerard Braad"](http://gravatar.com/avatar/e466994eea3c2a1672564e45aca844d0.png?s=60)](http://gbraad.nl "Gerard Braad <me@gbraad.nl>") |
|---|
| [@gbraad](https://twitter.com/gbraad)  |
